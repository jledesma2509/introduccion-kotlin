package com.continental.webinarapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Variables mutables
        var nombres = "Juan Jose"
        var edad = 30
        nombres = "Jose"

        //Variables immutables
        val pi = 3.1415

        val resultado = sumarOneLine(8,2)
        println("$resultado")

        println(longitudCadena("Hola"))
        val resultadoCadena = "Hola".longitudCadenaExtension()
        println(resultadoCadena)

        val resultadoSuma = 2 mas 4
        println("$resultadoSuma")

        var apellidos:String? = "Ledesma"

        //Elvis
        val contadorLetras = apellidos?.length ?: 0
        println("$contadorLetras")

        val resultadoKotlin = Util.sumar(10,20)
        println("$resultadoKotlin")

        val persona : PersonaKotlin? = PersonaKotlin("Juan Jose","Ledesma",30)

        persona?.let { personalibredeNull ->
            println("Hola ${personalibredeNull.nombres}")
        }

    }

    fun sumar(numero1:Int, numero2:Int) : Int {
        return numero1+numero2
    }

    /*
     public static int sumar(int numero1, int numero2){
        return numero1+numero2;
    }
     */

    fun sumarOneLine(numero1:Int, numero2:Int)  =  numero1+numero2

    fun longitudCadena(cadena:String) =  cadena.length

    fun String.longitudCadenaExtension() =  this.length

    fun Int.sumarExtension(numero2:Int)  =  this+numero2

    infix fun Int.mas(numero2:Int)  =  this+numero2
}